#!/usr/bin/python3
# encoding:utf8


import pygame
import random
from pygame import *
import time
from planeBattle import Manager

class Start_Button(object):
    def __init__(self, screen):
        self.mImage1 = pygame.image.load("./images/background.png")
        self.mImage2 = pygame.image.load("./images/again.png")
        self.mImage3 = pygame.image.load("./images/gameover.png")
        self.screen = screen

    def draw(self):
        self.screen.blit(self.mImage1, (0, 0))
        self.screen.blit(self.mImage2, (85, 400))
        self.screen.blit(self.mImage3, (85, 500))
        # self.screen.blit(self.mImage2, (0, self.y2))


class GameStart(object):
    bg_zise = (480, 700)

    def __init__(self):
        pygame.init()
        self.manager = Manager()
        self.screen = pygame.display.set_mode(GameStart.bg_zise, 0, 32)
        #self.background = pygame.image.load("./images/background.png")
        self.map = Start_Button(self.screen)

    def drawText(self, text, x, y,textHeight=30, fontColor = (255, 0, 0), backGroundColor = None):
        font_obj = pygame.font.Font('./config/FZShouJinShu-S10S.ttf', textHeight)
        text_obj = font_obj.render(text, True, fontColor, backGroundColor)
        text_rect = text_obj.get_rect()
        text_rect.topleft = (x, y)
        self.screen.blit(text_obj, text_rect)


    def exit(self):
        pygame.quit()
        exit()

    def main(self):

        while True:  # 死循环确保窗口一直显示

            #self.screen.blit(self.background, (0, 0))
            # self.map.move()
            self.map.draw()
            self.drawText('飞机大战', 80, 250, textHeight=80, fontColor=(55, 55, 55), backGroundColor=None)

            for event in pygame.event.get():  # 遍历所有事件
                if event.type == pygame.QUIT:  # 如果单击关闭窗口，则退出
                    self.exit()

                if event.type==pygame.MOUSEBUTTONDOWN and 85<=event.pos[0]<=385 and \
                        400<=event.pos[1]<=441: #判断鼠标位置以及是否摁了下去
                    print("check_restart")
                    self.manager.start_game()

                if event.type==pygame.MOUSEBUTTONDOWN and 85<=event.pos[0]<=385 and \
                        500<=event.pos[1]<=541: #判断鼠标位置以及是否摁了下去
                    print("check_gameover")
                    manager.exit()


            pygame.display.update()
            time.sleep(0.01)


if __name__ == '__main__':
    manager = GameStart()
    manager.main()

# pygame.quit()  # 退出pygame
