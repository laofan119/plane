# -*- coding: utf-8 -*-
import openpyxl
import os

game_config = {}

# game_config['basic'] = dict(
#     attack=10,
#     hp=50,
# )

def load_config(name = "a",SheetName = "SheetName"):
    if name == "a":
        return game_config
    name = name
    inwb = openpyxl.load_workbook('./config/' + name + '.xlsx')
    sheetnames = inwb.sheetnames  # 所有的sheet页名字 取配置名
    sheetNum = 0
    # print(name)
    for sheetname in sheetnames:
        if SheetName == sheetname:
            ws = inwb[sheetname]
            start_row = 5
            rows = ws.max_row
            cols = ws.max_column
            # print(rows,cols)

            config_dict = {}

            # print(ws.cell(5, 1).value)

            for r in range(start_row, rows + 1):

                config_dict_row = {}
                # print(r, ws.cell(r, 1).value)
                for c in range(1, cols):
                    config_dict_row[ws.cell(start_row-1, c).value] = ws.cell(r, c).value
                    # print(ws.cell(start_row-1, c).value)
                    config_dict['id_' + str(ws.cell(r, 1).value)] = config_dict_row#ws.cell(r, c).value  # 这就是单纯的某行的第一列是key 第二列是值
                    # print('id_' + str(ws.cell(r, 1).value))
                game_config[SheetName] = config_dict   # 这块就把上面的basic覆盖了
                game_config[SheetName] = config_dict   # 这块就把上面的basic覆盖了

                # print(sheetNum, game_config)
                sheetNum += 1
            # game_config[sheetnames[sheetNum]] = game_config

    return game_config

load_config()   # 执行下方法