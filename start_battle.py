#!/usr/bin/python3
# encoding:utf8


import pygame
import random
from pygame import *
from setting import load_config
import time

class hero_attr(object):
      def __init__(self):
            self.level = 10  #临时变量
            self.base_hero_attr = load_config('hero', 'hero')['hero']['id_1']
            # A = self.base_hero_attr['hero']
            # K = A['id_1']
            # level_exp = A['level_exp']
            self.hero_base_atk = self.base_hero_attr['base_atk']
            self.hero_base_def = self.base_hero_attr['base_def']
            self.hero_base_hp = self.base_hero_attr['base_hp']
            self.hero_base_speed = self.base_hero_attr['base_speed']
            self.hero_base_atk_speed = self.base_hero_attr['base_atk_speed']

            self.hero_base_hit = self.base_hero_attr['base_hit']
            self.hero_base_dodge = self.base_hero_attr['base_dodge']
            self.hero_base_crit = self.base_hero_attr['base_crit']
            self.hero_base_deCrit = self.base_hero_attr['base_deCrit']
            self.hero_base_crit_hurt = self.base_hero_attr['base_crit_hurt']
            self.hero_base_deCrit_hurt = self.base_hero_attr['base_deCrit_hurt']

            self.hero_add_atk = self.base_hero_attr['add_atk']
            self.hero_add_def = self.base_hero_attr['add_def']
            self.hero_add_hp = self.base_hero_attr['add_hp']
            self.hero_add_speed = self.base_hero_attr['add_speed']
            self.hero_add_atk_speed = self.base_hero_attr['add_atk_speed']

            self.hero_add_hit = self.base_hero_attr['add_hit']
            self.hero_add_dodge = self.base_hero_attr['add_dodge']
            self.hero_add_crit = self.base_hero_attr['add_crit']
            self.hero_add_deCrit = self.base_hero_attr['add_deCrit']
            self.hero_add_crit_hurt = self.base_hero_attr['add_crit_hurt']
            # self.hero_add_deCrit_hurt = self.base_hero_attr['add_deCrit_hurt']

            print('atk', self.hero_base_atk, self.hero_base_def, self.hero_base_hp, self.hero_base_speed, self.hero_base_atk_speed)

            self.base_hero_evo_attr = load_config('hero', 'evolution')['evolution']['id_1']
            # A = self.base_hero_attr['hero']
            # K = A['id_1']
            # level_exp = A['level_exp']
            self.hero_evo_atk = self.base_hero_evo_attr['add_atk']
            self.hero_evo_def = self.base_hero_evo_attr['add_def']
            self.hero_evo_hp = self.base_hero_evo_attr['add_hp']
            self.hero_evo_speed = self.base_hero_evo_attr['add_speed']
            self.hero_evo_atk_speed = self.base_hero_evo_attr['add_atk_speed']

            # self.hero_evo_hit = self.base_hero_evo_attr['add_hit']
            # self.hero_evo_dodge = self.base_hero_evo_attr['add_dodge']
            # self.hero_evo_crit = self.base_hero_evo_attr['add_crit']
            # self.hero_evo_deCrit = self.base_hero_evo_attr['add_deCrit']
            # self.hero_evo_crit_hurt = self.base_hero_evo_attr['add_crit_hurt']
            # self.hero_evo_deCrit_hurt = self.base_hero_evo_attr['add_deCrit_hurt']

            # print('atk', self.hero_base_atk, self.hero_base_def, self.hero_base_hp, self.hero_base_speed,
            #       self.hero_base_atk_speed)
            self.hero_atk = self.hero_base_atk + (self.level-1) * self.hero_add_atk + self.hero_evo_atk
            self.hero_def = self.hero_base_def + (self.level - 1) * self.hero_add_def + self.hero_evo_def
            self.hero_hp = self.hero_base_hp + (self.level - 1) * self.hero_add_hp + self.hero_evo_hp
            self.hero_speed = self.hero_base_speed + (self.level - 1) * self.hero_add_speed + self.hero_evo_speed
            self.hero_atk_speed = self.hero_base_atk_speed + (self.level - 1) * self.hero_add_atk_speed + self.hero_evo_atk_speed

            # self.hero_hit = self.hero_base_hit + (self.level - 1) * self.hero_add_hit + self.hero_evo_hit
            # self.hero_dodge = self.hero_base_dodge + (self.level - 1) * self.hero_add_dodge + self.hero_evo_dodge
            # self.hero_crit = self.hero_base_crit + (self.level - 1) * self.hero_add_crit + self.hero_evo_crit
            # self.hero_deCrit = self.hero_base_deCrit + (self.level - 1) * self.hero_add_deCrit + self.hero_evo_deCrit
            # self.hero_crit_hurt = self.hero_base_crit_hurt+ (self.level - 1) * self.hero_add_crit_hurt + self.hero_evo_crit_hurt
            # self.hero_deCrit_hurt = self.hero_base_deCrit_hurt + (self.level - 1) * self.hero_add_deCrit_hurt + self.hero_evo_deCrit_hurt



            # self.temp_hero_hit = self.hero_hit
            # self.temp_hero_dodge = self.hero_dodge
            # self.temp_hero_crit = self.hero_crit
            # self.temp_hero_deCrit = self.hero_deCrit
            # self.temp_hero_crit_hurt = self.hero_crit_hurt
            # self.temp_hero_deCrit_hurt = self.hero_deCrit_hurt

            # print(self.temp_hero_atk, self.hero_atk, self.temp_hero_def, self.hero_def, self.temp_hero_hp, self.hero_hp, self.temp_hero_speed, self.hero_speed, self.temp_hero_atk_speed, self.hero_atk_speed)

class enemy_attr(object):
      def __init__(self):
            self.enemy_attr = load_config('enemy', 'enemy')['enemy']['id_100001']
            # A = self.base_hero_attr['hero']
            # K = A['id_1']
            # level_exp = A['level_exp']
            self.enemy_atk = self.enemy_attr['atk']
            self.enemy_def = self.enemy_attr['def']
            self.enemy_hp = self.enemy_attr['hp']
            self.enemy_speed = self.enemy_attr['speed']
            self.enemy_atk_speed = self.enemy_attr['atk_speed']
# for A in a:
#     for B in a:
        # print(B)
        # S = getattr(B, "get_exp")
        # print(S)
class battle(object):
      def __init__(self):
            self.hero_attr = hero_attr()
            self.enemy_attr = enemy_attr()
            self.temp_hero_atk = self.hero_attr.hero_atk
            self.temp_hero_def = self.hero_attr.hero_def
            self.temp_hero_hp = self.hero_attr.hero_hp
            self.temp_hero_speed = self.hero_attr.hero_speed
            self.temp_hero_atk_speed = self.hero_attr.hero_atk_speed

            self.temp_enemy_atk = self.enemy_attr.enemy_atk
            self.temp_enemy_def = self.enemy_attr.enemy_def
            self.temp_enemy_hp = self.enemy_attr.enemy_hp
            self.temp_enemy_speed = self.enemy_attr.enemy_speed
            self.temp_enemy_atk_speed = self.enemy_attr.enemy_atk_speed

            # print('hero', self.temp_hero_atk, self.temp_hero_def, self.temp_hero_hp, self.temp_hero_speed,self.temp_hero_atk_speed,
            #       'enemy', self.temp_enemy_atk, self.temp_enemy_def, self.temp_enemy_hp, self.temp_enemy_speed,self.temp_enemy_atk_speed)

      def attack(self,skill_id):
            if skill_id == 1:
                  self.hero_hurt = self.temp_hero_atk - self.temp_enemy_def
                  if self.hero_hurt <= 10:
                        self.hero_hurt = 10
                  print('hero_hurt', self.hero_hurt)
            return self.hero_hurt

      def deAttack(self, skill_id):
            if skill_id == 1:
                  self.enemy_hurt = self.temp_enemy_atk - self.temp_hero_def
                  if self.enemy_hurt <= 10:
                        self.enemy_hurt = 10
                  print('enemy_hurt', self.enemy_hurt)
            return self.enemy_hurt


if __name__ == '__main__':
    hero_attr()
    enemy_attr()
