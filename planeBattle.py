#!/usr/bin/python3
# encoding:utf8


import pygame
import random
from pygame import *
import time
from start_battle import battle
# from start_battle import hero_attr


class HeroPlane(pygame.sprite.Sprite):
    bullets = pygame.sprite.Group()
    def __init__(self, screen):
        pygame.sprite.Sprite.__init__(self)

        self.hero = pygame.image.load("./images/me1.png")
        self.rect = self.hero.get_rect()
        self.rect.topleft = [250, 580]
        #self.hero_X = 250
        #self.hero_Y = 580
        self.speed = 5
        self.atkSpeed = 10
        self.screen = screen
        self.sound = GameSound()
        #print("初始坐标", self.hero_X, self.hero_Y)

        self.bullets = pygame.sprite.Group()
    def key_control(self):
        key_pressed = pygame.key.get_pressed()

        if (key_pressed[K_a] or key_pressed[K_LEFT]) and self.rect.left >= 0:
            self.rect.left -= self.speed
            #print("左")
        if (key_pressed[K_d] or key_pressed[K_RIGHT]) and self.rect.right <= 480:
            self.rect.right += self.speed
            #print("右")
        if (key_pressed[K_w] or key_pressed[K_UP]) and self.rect.top >= 0:
            self.rect.top -= self.speed
            #print("上")
        if (key_pressed[K_s] or key_pressed[K_DOWN]) and self.rect.bottom <= 700:
            self.rect.bottom += self.speed
            #print("下")
        if key_pressed[K_SPACE]:
            print("发射")
            # self.screen.blit(self.hero, (self.hero_X, self.hero_Y))
            bullet = Bullet(self.screen, self.rect.left, self.rect.top)
            self.bullets.add(bullet)
            self.sound.hero_fire_sound()
            HeroPlane.bullets.add(bullet)

    def auto_atk(self):
        self.atkSpeedtemp = self.atkSpeed
        if self.atkSpeedtemp > 0:
            # time.sleep(self.atkSpeedtemp/10)
            bullet = Bullet(self.screen, self.rect.left, self.rect.top)
            self.bullets.add(bullet)
            self.sound.hero_fire_sound()
            HeroPlane.bullets.add(bullet)


    def update(self):
        self.key_control()
        self.display()
        # self.auto_atk()

    def display(self):
        self.screen.blit(self.hero, self.rect)
        self.bullets.update()
        self.bullets.draw(self.screen)
        #for bullet in self.bullets:
        #    bullet.auto_move()
        #    bullet.display()

    @classmethod
    def clear_bullets(cls):
        cls.bullets.empty()



class Bullet(pygame.sprite.Sprite):
    def __init__(self, screen, X, Y):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.image.load("./images/bullet1.png")
        self.rect = self.image.get_rect()
        self.rect.topleft = [X+51, Y+6]
        self.Bullet_speed = 6
        self.screen = screen

    def update(self):
        self.rect.top -= self.Bullet_speed
        if self.rect.bottom <= 0:
            self.kill()
        # self.rect.left = self.rect.left+50
        # self.rect.top = self.rect.top-6
        #
        #print("初始坐标", self.X, self.Y)

    # def display(self):
    #     self.screen.blit(self.Bullet, (self.rect.left, self.rect.top))
    # def auto_move(self):
    #     self.rect.top -= self.Bullet_speed

class EnemyPlane(pygame.sprite.Sprite):
    enemybullets = pygame.sprite.Group()
    monters = pygame.sprite.Group()
    def __init__(self, screen):
        pygame.sprite.Sprite.__init__(self)
        self.enemy = pygame.image.load("./images/enemy1.png")

        self.rect = self.enemy.get_rect()
        x = random.randrange(1, Manager.bg_zise[0], 50)
        self.rect.topleft = [x, 0]

        #self.enemy_X = 0
        #self.enemy_Y = 0
        self.atkSpeed = 10
        self.enemy_speed = 1
        self.screen = screen
        self.direction = 'right'
        self.sound = GameSound()
        #print("初始坐标", self.enemy_X, self.enemy_Y)

        self.enemybullets = pygame.sprite.Group()
        self.monters = pygame.sprite.Group()

    def auto_move(self):

        if self.direction == 'right':
            #print("怪物坐标", self.rect.left)
            self.rect.right += self.enemy_speed
            #print("怪物坐标", self.rect.left)
        elif self.direction == 'left':
            #print("怪物坐标", self.rect.left)
            self.rect.right -= self.enemy_speed
            #print("怪物坐标", self.rect.left)

        if self.rect.right <= 0:
            self.direction = 'right'
        elif self.rect.right >= 433:
            self.direction = 'left'
        else:
            self.direction = self.direction

        self.rect.bottom += self.enemy_speed

    def auto_atk(self):
        if random.randint(0,1000/self.atkSpeed) == 1:
            enemybullet = enemy_Bullet(self.screen, self.rect.left, self.rect.top)
            self.sound.enemy_fire_sound()
            self.enemybullets.add(enemybullet)
            EnemyPlane.enemybullets.add(enemybullet)

    @classmethod
    def clear_bullets(cls):
        cls.enemybullets.empty()

    @classmethod
    def clear_monters(cls):
        cls.monters.empty()


    def update(self):
        # self.key_control()
        self.auto_move()
        self.auto_atk()
        self.display()
        # for event in pygame.event.get():  # 遍历所有事件
        #     if event.type == Manager.create_enemy_id:
        #         self.kill()

    def display(self):
        #self.screen.blit(self.enemy, (self.rect.left, self.rect.bottom))

        self.screen.blit(self.enemy, self.rect)
        self.enemybullets.update()
        self.enemybullets.draw(self.screen)
        #for enemy_Bullet in self.enemybullets:
        #    enemy_Bullet.auto_move()
        #    enemy_Bullet.display()


class enemy_Bullet(pygame.sprite.Sprite):
    def __init__(self, screen,X ,Y):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.image.load("./images/bullet2.png")
        self.rect = self.image.get_rect()
        self.rect.topleft = [X+28, Y+40]
        self.Bullet_speed = 6
        self.screen = screen

    def update(self):
        self.rect.top += self.Bullet_speed
        if self.rect.top >= 722:
            self.kill()
        # self.X = X+28
        # self.Y = Y+43
        #
        #
        #print("敌方子弹初始坐标", self.X, self.Y)

    # def display(self):
    #     self.screen.blit(self.enemy_Bullet, (self.rect.left, self.rect.bottom))
    # def auto_move(self):
    #     self.rect.bottom += self.Bullet_speed



class Boom(object):
    def __init__(self, screen, type, isboom):
        self.screen = screen
        self.isboom = isboom

        if isboom == True:
            self.image_id = 5
        else:
            self.image_id = 2
        if type == "enemy":
            self.mImage = [pygame.image.load("./images/enemy1_down"+ str(v) + ".png") for v in range (1,self.image_id)]
            self.mImage1 = [pygame.image.load("./images/enemy1_down1.png")]
        else:
            self.mImage = [pygame.image.load("./images/me_destroy_"+ str(v) + ".png") for v in range (1,self.image_id)]
            self.mImage1 = [pygame.image.load("./images/me_destroy_1.png")]

        print('boom', self.screen, isboom, type)
        self.mIndex = 0
        self.mPos = [0, 0]
        self.mVisible = False

    def action(self, rect):
        self.mPos[0] = rect.left
        self.mPos[1] = rect.top
        self.mVisible = True

    def draw(self):
        if not self.mVisible:
            return

        if self.isboom == True:
            self.screen.blit(self.mImage[self.mIndex], (self.mPos[0], self.mPos[1]))
            self.mIndex += 1
            if self.mIndex >= len(self.mImage):
                self.mIndex = 0
                self.mVisible = False
        else:
            self.screen.blit(self.mImage[self.mIndex], (self.mPos[0], self.mPos[1]))
            self.mIndex = 0
            self.mVisible = True
            self.screen.blit(self.mImage1[self.mIndex], (self.mPos[0], self.mPos[1]))
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~")

# class Hit(object):
#     def __init__(self,screen,type):
#         self.screen = screen
#         if type == "enemy":
#             self.mImage = [pygame.image.load("./images/enemy1_down"+ str(v) + ".png") for v in range (1,2)]
#             self.mImage1 = [pygame.image.load("./images/enemy1_down1.png")]
#         else:
#             self.mImage = [pygame.image.load("./images/me_destroy_"+ str(v) + ".png") for v in range (1,2)]
#             self.mImage1 = [pygame.image.load("./images/me_destroy_1.png")]
#
#         self.mIndex = 0
#         self.mPos = [0, 0]
#         # self.mVisible = False
#
#     def action(self,rect):
#         self.mPos[0] = rect.left
#         self.mPos[1] = rect.top
#         # self.mVisible = True
#
#     def draw(self):
#         # if not self.mVisible:
#         #     return
#         self.screen.blit(self.mImage[self.mIndex],(self.mPos[0],self.mPos[1]))
#         self.screen.blit(self.mImage1[self.mIndex], (self.mPos[0], self.mPos[1]))
#         self.mIndex += 1
#         if self.mIndex >= len(self.mImage):
#             self.mIndex = 0
            # self.mVisible = False

class GameSound(object):
    def __init__(self):
        pygame.mixer.init()
        pygame.mixer.music.load('./sound/Bg_music01.wav')
        self.__boom = pygame.mixer.Sound('./sound/Boom.wav')
        self.__hero_fire = pygame.mixer.Sound('./sound/hero_fire.wav')
        self.__enemy_fire = pygame.mixer.Sound('./sound/enemy_fire.wav')
        pygame.mixer.music.set_volume(1.5)
        # pygame.mixer.Sound.set_volume(0.5)

    def playBGM(self):
        pygame.mixer.music.play(-1)

    def Boom_BGM(self):
        pygame.mixer.Sound.play(self.__boom)

    def hero_fire_sound(self):
        pygame.mixer.Sound.play(self.__hero_fire)

    def enemy_fire_sound(self):
        pygame.mixer.Sound.play(self.__enemy_fire)

class GameBackGround(object):
    def __init__(self,screen):
        self.mImage1 = pygame.image.load("./images/background.png")
        self.mImage2 = pygame.image.load("./images/background.png")
        self.screen = screen
        self.y1 = 0
        self.y2 = -Manager.bg_zise[1]

    def move(self):
        self.y1 += 2
        self.y2 += 2
        if self.y1 >= Manager.bg_zise[1]:
            self.y1 = 0
        if self.y2 >= Manager.bg_zise[1]:
            self.y2 = -Manager.bg_zise[1]

    def draw(self):
        self.screen.blit(self.mImage1, (0, self.y1))
        self.screen.blit(self.mImage2, (0, self.y2))

class herolife(pygame.sprite.Sprite):
    def __init__(self,screen,herolifeNum):
        pygame.sprite.Sprite.__init__(self)
        self.herolifeNum = herolifeNum
        self.screen = screen
        self.mImage = pygame.image.load("./images/life.png")
        self.rect = self.mImage.get_rect()


    def draw(self):
        self.first_life = self.screen.blit(self.mImage, (Manager.bg_zise[0] - self.rect.right, 0))
        # self.secend_life = self.screen.blit(self.mImage, (Manager.bg_zise[0] - self.rect.right * 2, 0))
        # self.third_life = self.screen.blit(self.mImage, (Manager.bg_zise[0] - self.rect.right * 3, 0))
        # print(Manager.bg_zise[0] - self.rect.left,Manager.bg_zise[0],self.rect.right)
        # self.first_life()
        # self.secend_life()
        # self.third_life()


    def update(self,herolifeNum = 3):
        # self.herolifeNum -= 1
        print(herolifeNum)
        if self.herolifeNum == 2:
            self.kill()



class Manager(object):
    bg_zise = (480, 700)
    create_enemy_id = 10
    game_over_id = 11
    is_game_over = False
    over_time = 3
    herolifeNum = 3
    def __init__(self):

        pygame.init()
        self.herolifeNum = Manager.herolifeNum
        # self.gameStart = GameStart()
        self.screen = pygame.display.set_mode(Manager.bg_zise, 0, 32)
        #self.background = pygame.image.load("./images/background.png")
        self.map = GameBackGround(self.screen)

        self.herolife = herolife(self.screen,self.herolifeNum)

        self.heros = pygame.sprite.Group()
        self.monsters = pygame.sprite.Group()
        self.monsters_buttles = pygame.sprite.Group()

        self.hero_boom =Boom(self.screen, 'player', True)
        self.enemy_boom = Boom(self.screen, 'enemy', True)
        self.hero_Hit = Boom(self.screen, 'player', False)
        self.enemy_Hit = Boom(self.screen, 'enemy', False)
        # self.hero_Hit = Hit(self.screen, 'player')
        # self.enemy_Hit = Hit(self.screen, 'enemy')
        self.sound = GameSound()

    def exit(self):
        pygame.quit()
        exit()

    def show_time_text(self):
        self.drawText('Gameover %d'%Manager.over_time,100,Manager.bg_zise[1]/2,textHeight = 50,fontColor = [255, 0, 0])

    def game_over_timer(self):
        self.show_time_text()
        Manager.over_time -= 1
        self.start_game()
        if Manager.over_time == 0:
            pygame.time.set_timer(Manager.game_over_id,0)
            Manager.over_time = 3
            Manager.is_game_over = False


    def start_game(self):
        HeroPlane.clear_bullets()
        EnemyPlane.clear_bullets()
        EnemyPlane.clear_monters()
        self.main()

    def new_hero(self):
        hero = HeroPlane(self.screen)
        self.heros.add(hero)

    def new_enemy(self):
        enemy = EnemyPlane(self.screen)
        self.monsters.add(enemy)
        # print("monster",self.monsters,"enemy",enemy)

    def drawText(self, text, x, y,textHeight=30, fontColor = (255, 0, 0), backGroundColor = None):
        font_obj = pygame.font.Font('./config/FZShouJinShu-S10S.ttf', textHeight)
        text_obj = font_obj.render(text, True, fontColor, backGroundColor)
        text_rect = text_obj.get_rect()
        text_rect.topleft = (x, y)
        self.screen.blit(text_obj, text_rect)



    def main(self):
        # self.hero_attr = hero_attr()
        self.battle = battle()
        self.new_hero()
        # self.new_enemy()
        pygame.time.set_timer(Manager.create_enemy_id, 1000)

        self.sound.playBGM()

        self.hero_hurt = 0
        self.enemy_hurt = 0
        self.temp_hero_hp = self.battle.temp_hero_hp
        self.temp_enemy_hp = self.battle.temp_enemy_hp


        while True:  # 死循环确保窗口一直显示
            #self.screen.blit(self.background, (0, 0))
            self.map.move()
            self.map.draw()
            self.herolife.draw()


            if Manager.is_game_over:
                self.show_time_text()
            self.drawText('飞机大战', 0, 0)


            iscollide = pygame.sprite.groupcollide(self.heros, self.monsters, True ,True)

            if iscollide:
                Manager.is_game_over = True
                pygame.time.set_timer(Manager.game_over_id, 1000)
                items = list(iscollide.items())[0]
                x = items[0]
                y = items[1][0]

                self.hero_hurt = self.battle.attack(1)
                self.enemy_hurt = self.battle.deAttack(1)
                self.temp_hero_hp = self.battle.temp_hero_hp
                self.temp_enemy_hp = self.battle.temp_enemy_hp
                self.temp_hero_hp = self.temp_hero_hp - self.hero_hurt
                self.temp_enemy_hp = self.temp_enemy_hp - self.enemy_hurt

                print('temp_hero_hp', self.temp_hero_hp, 'temp_enemy_hp', self.temp_enemy_hp)

                if self.temp_hero_hp <= 0:
                    self.hero_boom.action(x.rect)
                    self.sound.Boom_BGM()
                else:
                    self.hero_Hit.action(x.rect)
                    self.sound.Boom_BGM()

                if self.temp_enemy_hp <= 0:
                    self.enemy_boom.action(y.rect)
                    self.sound.Boom_BGM()
                else:
                    self.enemy_Hit.action(x.rect)
                    self.sound.Boom_BGM()
                self.hero_boom.draw()
                self.enemy_boom.draw()

            is_attacking = pygame.sprite.groupcollide(self.heros, EnemyPlane.enemybullets, True, True)
            if is_attacking:
                Manager.is_game_over = True
                pygame.time.set_timer(Manager.game_over_id, 1000)
                items = list(is_attacking.items())[0]
                x = items[0]

                # self.hero_hurt = self.battle.attack(1)
                self.enemy_hurt = self.battle.deAttack(1)
                # self.temp_hero_hp = self.battle.temp_hero_hp
                self.temp_enemy_hp = self.battle.temp_enemy_hp
                # self.temp_hero_hp = self.temp_hero_hp - self.hero_hurt
                self.temp_enemy_hp = self.temp_enemy_hp - self.enemy_hurt

                print('temp_enemy_hp', self.temp_enemy_hp)

                if self.temp_enemy_hp <= 0:
                    self.hero_boom.action(x.rect)
                    self.sound.Boom_BGM()
                else:
                    self.hero_Hit.action(x.rect)
                    self.sound.Boom_BGM()
                self.hero_boom.draw()
                self.enemy_boom.draw()
                # self.hero_boom.action(x.rect)
                # HeroPlane.clear_bullets()
                # EnemyPlane.clear_bullets()
                # EnemyPlane.clear_monters()
                # self.sound.Boom_BGM()
                # self.gameStart.main()
                # self.herolifeNum -= 1
                # self.herolife.update(self.herolifeNum)

            is_Be_attacked = pygame.sprite.groupcollide(self.monsters, HeroPlane.bullets, True, True)
            if is_Be_attacked:
                items = list(is_Be_attacked.items())[0]

                y = items[1][0]

                # self.hero_hurt = self.battle.attack(1)
                self.enemy_hurt = self.battle.deAttack(1)
                # self.temp_hero_hp = self.battle.temp_hero_hp
                self.temp_enemy_hp = self.battle.temp_enemy_hp
                # self.temp_hero_hp = self.temp_hero_hp - self.hero_hurt
                self.temp_enemy_hp = self.temp_enemy_hp - self.enemy_hurt

                print('temp_hero_hp~~~~~', self.temp_hero_hp)

                if self.temp_hero_hp <= 0:
                    self.enemy_boom.action(y.rect)
                    self.sound.Boom_BGM()
                else:
                    self.enemy_Hit.action(y.rect)
                    self.sound.Boom_BGM()
                    print('IsHit~~~~~', self.temp_hero_hp)
                self.hero_boom.draw()
                self.enemy_boom.draw()
                # self.enemy_boom.action(y.rect)


                # self.sound.Boom_BGM()
                # self.herolifeNum -= 1
                # self.herolife.update(self.herolifeNum)
                # self.gameStart.main()
            for event in pygame.event.get():  # 遍历所有事件
                if event.type == pygame.QUIT:  # 如果单击关闭窗口，则退出
                    self.exit()

                elif event.type == Manager.create_enemy_id:
                    self.new_enemy()

                elif event.type == Manager.game_over_id:
                    self.game_over_timer()


            self.heros.update()
            self.monsters.update()
            pygame.display.update()
            time.sleep(0.01)


if __name__ == '__main__':
    manager = Manager()
    manager.main()

# pygame.quit()  # 退出pygame
